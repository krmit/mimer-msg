# mimnger-msg

## Install

```
$ npm install
```

## Usage

```
$ npm run start
```

## License

AGPL, see the file COPYING.

## Contribute

Please feel free to open an issue or submit a pull request.

## Changelog

- **0.0.1** _DATE_ First version published

## Author

**©Magnus Kronnäs 2024 [magnus.kronnas.se](magnus.kronnas.se)**
