#!/usr/bin/env node
import express from "express";
import { logger } from "./logger.js";
import { notFound, serverError } from "./errors.js";
export { hello } from "./hello.js";
import mysql from "mysql2/promise";
import crypto from "crypto";
import { v4 as uuid } from "uuid";

type TokenData = { [key: string]: string };
const token_storage: TokenData = {};

async function main() {
  const app = express();

  const db = await mysql.createConnection({
    host: "localhost",
    user: "admin",
    password: "abc123",
    database: "MimerMsg",
  });

  const result = await db.execute("SELECT 1+1;");

  app.use(logger);

  // If you need to parse url.
  //app.use(express.urlencoded({ extended: true }));

  app.get("/hello/:world", function (req, res) {
    res.send("Hello " + req.params.world + "!");
  });

  app.get("/msg", async function (req, res) {
    let question = "SELECT * FROM msg;";
    console.log(question);
    const result = await db.execute(question);
    const data = result.values().next().value;
    res.send(data);
  });

  app.get("/input", async function (req, res) {
    const msg = req.query.msg;
    const user = "krm";
    const created = "2000-01-01 10:30:00";
    const question = "INSERT INTO msg VALUES (null, ?, ?, ?);";
    const result = await db.execute(question, [msg, created, user]);
    res.send("OK!");
  });

  app.get("/login", async function (req, res) {
    if (
      req.query &&
      typeof req.query.user === "string" &&
      typeof req.query.password === "string"
    ) {
      const user_name = req.query.user;
      const password = crypto
        .createHash("md5")
        .update(String(req.query.password))
        .digest("hex");
      let question = "SELECT name,password FROM user WHERE name =?";
      console.log(user_name);
      console.log(req.query.password);
      console.log(db.format(question, [user_name]));
      const user_answer = await db.execute(question, [user_name]);
      const password_from_database = user_answer.values().next()
        .value[0].password;
      console.log(password_from_database);
      if (password_from_database === password) {
        const token = uuid();
        console.log(token);
        token_storage[token] = user_name;
        res.cookie("login", token, { maxAge: 10000000 });
        res.send("Du är inloggad!");
      } else {
        res.send("Fel lösenord!");
      }
    } else {
      res.send("Du har inte skrivit in anvädnarnamn eller lösenord!");
    }
  });

  app.get("/error", function (req, res) {
    throw "Test throwing Error";
  });

  app.use("/", express.static("public"));

  app.use(notFound);

  app.use(serverError);

  const port = 8080;

  app.listen(port, () => {
    console.log("You can find this server on: http://localhost:" + port);
  });
}

main();
